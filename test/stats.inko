import std::time::Duration

import test::assert::Assert

object Stats {
  def init(pass: Integer, fail: Integer, skip: Integer, dur: Duration) {
    let @pass = pass
    let @fail = fail
    let @skip = skip
    let @dur = dur
  }

  def from_assert(assert: Assert, dur: Duration) -> Self {
    assert.skipped?
      .if_true {
        return new(0, 0, 1, dur)
      }
    assert.failed?
      .if_true {
        return new(0, 1, 0, dur)
      }
    new(1, 0, 0, dur)
  }

  def pass -> Integer {
    @pass
  }

  def fail -> Integer {
    @fail
  }

  def skip -> Integer {
    @skip
  }

  def duration -> Duration {
    @dur
  }
}
